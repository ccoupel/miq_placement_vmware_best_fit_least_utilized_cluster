#
# Description: This method is used to find all hosts, datastores that are the least utilized
#
module ManageIQ
  module Automate
    module Infrastructure
      module VM
        module Provisioning
          module Placement
            class VmwareBestFitLeastUtilized
              def initialize(handle = $evm)
                @handle = handle
              end

              def main
                @handle.log("info", "vm=[#{vm.name}], space required=[#{vm.provisioned_storage}]")
                best_fit_least_utilized
              end

              private

              def request
                @request ||= @handle.root["miq_provision"].tap do |req|
                  ManageIQ::Automate::System::CommonMethods::Utils::LogObject.log_and_raise('miq_provision not specified', @handle) if req.nil?
                end
              end

              def vm
                @vm ||= request.vm_template.tap do |vm|
                  ManageIQ::Automate::System::CommonMethods::Utils::LogObject.log_and_raise('VM not specified', @handle) if vm.nil?
                end
              end

              def storage_profile_id
                @storage_profile_id ||= request.get_option(:placement_storage_profile).tap do |sp|
                  @handle.log("info", "Selected storage_profile_id: #{sp}") if sp
                end
              end

              def clear_host
                request.set_option(:placement_host_name, [nil, nil])
              end

              def best_fit_least_utilized
                host = storage = min_registered_vms = nil
                storage_cluster=request.get_option(:storage_cluster)
                @handle.log("info", "vm=[#{vm.name}] storage_cluster=#{storage_cluster}")

                request.eligible_hosts.select { |h| !h.maintenance && h.power_state == "on" }.each do |h|
                  next if min_registered_vms && h.vms.size >= min_registered_vms

                  # Setting the host to filter eligible storages
                  request.set_host(h)
                                  @handle.log("info", "vm=[#{vm.name}] host:#{h}")
                  storages = request.eligible_storages
                                  @handle.log("info", "vm=[#{vm.name}] #{storages.count rescue 0} eligible datastores")
                  
                  storages.select! { |s| s.storage_clusters.select{|c| 
                        @handle.log(:info, "    CC- #{s.name}: #{c.name}==#{storage_cluster} => #{c.name==storage_cluster}")
                        c.name==storage_cluster}.count >0} unless storage_cluster.blank?
                                  @handle.log("info", "vm=[#{vm.name}] #{storages.count rescue 0} left with storage_cluster=#{storage_cluster}")

                  # Filter out storages that do not have enough free space for the Vm
                  storages.select! { |s| 
                    @handle.log(:info, "    CC- #{s.name}: #{s.free_space} > #{vm.provisioned_storage} => #{s.free_space > vm.provisioned_storage}")
                    s.free_space > vm.provisioned_storage }
                                  @handle.log("info", "vm=[#{vm.name}] #{storages.count rescue 0} left datastores with free space > #{vm.provisioned_storage}")

                  storages.select! { |s| 
                     @handle.log(:info, "    CC- #{s.name}: #{s.storage_profiles.pluck(:id)}.include?(#{storage_profile_id} => #{s.storage_profiles.pluck(:id).include?(storage_profile_id)}")
                    s.storage_profiles.pluck(:id).include?(storage_profile_id) } if storage_profile_id
                                  @handle.log("info", "vm=[#{vm.name}] #{storages.count rescue 0} left with profile include #{storage_profile_id}")


                  s = storages.max_by(&:free_space) rescue nil
                  next if s.nil?
                  host    = h
                  storage = s
                  min_registered_vms = h.vms.size
                end

                # Set host and storage
                host ? request.set_host(host) : clear_host
                request.set_storage(storage) if storage

                @handle.log("info", "vm=[#{vm.name}] host=[#{host}] storage=[#{storage}]")
              end
            end
          end
        end
      end
    end
  end
end

ManageIQ::Automate::Infrastructure::VM::Provisioning::Placement::VmwareBestFitLeastUtilized.new.main
